import { createTheme } from '@mui/material/styles';

const theme = createTheme({
    palette: {
        primary: {
            main: '#1976d2',
        },
        secondary: {
            main: '#dc004e',
        },
    },
});
export const palette = {
    $primary: '#364F6B',
    $background: '#FFFFFF',
    $secondary: '#364F6B',
    $primary_normal: '#04002E',
    $primary_text: '#595A5C',
    $primary_tables: '#04002E',
}

export default theme;
