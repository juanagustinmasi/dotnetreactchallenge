import { AppBar, Box, Button, Container, Grid, Toolbar, Typography } from '@mui/material';
import * as React from 'react';
import { Outlet, useNavigate } from 'react-router-dom';
import logo from 'assets/logo.png'
import useStyles from './style';
interface PagesModel {
  name: string,
  route: string,
}

const pages: PagesModel[] = [
  { name: 'Home', route: '/home' },

];
const settings: PagesModel[] = [
  { name: 'Home', route: '/home' },
]
const Layout = (props: any) => {
  const classes = useStyles();
  const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(null);
  const navigate = useNavigate();
  return (
    <>
      <AppBar position="fixed" color={'secondary'}>
        <Container maxWidth="xl">
          <Toolbar className={classes.toolbar} disableGutters>
            <Typography
              variant="h6"
              noWrap
              component="div"
              sx={{ flexGrow: 12, mr: 2, display: { xs: 'none', md: 'flex' } }}
            >
              <img className={classes.logo} src={logo}></img>
            </Typography>

            {/**Pantalla responsive */}
            <Typography
              variant="h6"
              noWrap
              component="div"
              sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}
            >
              <img className={classes.logo} src={logo}></img>
            </Typography>
            {/**Fin Pantalla responsive */}

            <Box
              sx={{
                display: { xs: 'none', md: 'flex' },
                marginLeft: 'auto',
              }}
            >
              {pages.map((page, index) => (
                <Button
                  key={index}
                  onClick={() => navigate(page.route)}
                  className={classes.toolbarButtons}
                >
                  {page.name}
                </Button>
              ))}
            </Box>

          </Toolbar>
        </Container>
      </AppBar>
      <div style={{ height: '8rem' }}></div>
      {/* childrens */}
      <div id='outlet'><Outlet></Outlet></div>
    </>
  );
};
export default Layout;
