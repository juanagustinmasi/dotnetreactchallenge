import { Theme } from '@mui/material';
import { createStyles, makeStyles } from '@mui/styles';
import { palette } from 'theme';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        toolbar: {
            height: '7.37rem',
        },
        toolbarButtons: {
            color: 'white!important',
            display: 'block',
            fontSize: '1rem',
            letterSpacing: 0,
            textTransform: 'capitalize',
            /*             marginLeft: '.5rem',
                        marginRight: '.5rem', */
        },
        menuFrame: {
            '& .MuiList-root': {
                background: palette.$primary,
                color: 'white',

            },
            '& .MuiPaper-root': {
                borderRadius: '0.625rem',
            }
        },
        footer: {
            marginBottom: 'auto',
            bottom: 0,
            left: 0,
            width: 'auto',
            background: palette.$primary_normal,
            opacity: 1,
            display: 'flex',
            position: 'inherit',
            justifyContent: 'center',
            alignItems: 'center',
            height: '14rem',
        },
        logo: {
            maxWidth: `6rem`
        },
        menuResponsive: {
            '& .MuiList-root': {
                background: palette.$primary,
                color: palette.$primary_text,
            },
            '& .MuiPaper-root': {
                borderRadius: '0.625rem',
            }
        }
    })
);
export default useStyles;