import { Box, Chip, Grid, InputAdornment, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow, TextField, ThemeProvider, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import { palette } from "theme";
import useStyles from "./style";
import { personTableColumns } from "models/DataTableModel";
import * as personApi from "API/person";
import { PersonModel } from "models/PersonModel";
import { useNavigate } from "react-router-dom";


const Person = (prop: any) => {
    const classes = useStyles();
    const [personId, setPersonId] = useState();
    const [data, setData] = useState<PersonModel[]>();
    const navigate = useNavigate();
    useEffect(() => {
        const fetch = async () => {
            try {
                const response = await personApi.getAllPersons();
                setData(response);
                console.log(response);
            } catch (error) {
                console.log("error getting persons", error);
            }
        };
        fetch();
    }, []);

    const handleUpdate = (id: string, row: any) => {
        navigate(`/update/${id}`, { state: row })

    }

    return (
        <>
            <Grid container justifyContent='center' alignItems={'center'}>
                <Grid item>
                    <Typography className={classes.title}> <strong>¡Welcome to the challenge!</strong></Typography>
                </Grid>
            </Grid>

            <Grid container justifyContent={'center'} >
                <Grid container item justifyContent={'center'} xs={12} pl={6} pr={6} >
                    <Box border={`solid ${palette.$primary_tables} 3px `} borderRadius={'10px'} justifyContent={'center'}>
                        <Grid item container xs={12}>
                            <Grid item xs={12}>
                                <Typography textAlign={'center'} style={{ background: palette.$primary_tables, color: 'whitesmoke' }}> Search by person</Typography>
                            </Grid>

                            <Grid item xs={12} md={12} textAlign={'center'}>
                                <TextField
                                    id={'personId'}
                                    type={'text'}
                                    size={'small'}
                                    margin={'normal'}
                                    placeholder='Juan Masi...'
                                    helperText={'Search by person'}
                                    variant="standard"
                                    InputProps={{
                                        style: { background: 'white' },
                                        startAdornment: (
                                            <InputAdornment position="start">
                                                <search />
                                            </InputAdornment>
                                        )
                                    }}
                                    onChange={(e: any) => setPersonId(e.target.value)}
                                ></TextField>
                            </Grid>

                        </Grid>
                    </Box>

                    <div style={{ height: '1rem', width: '100%' }}></div>
                    <div style={{ width: '100%', justifyContent: 'center', display: 'flex' }}>
                        <Grid m={{ xs: 2, sm: 0 }} item xs={12}>
                            <TableContainer component={Paper} className={classes.tableContainer}>
                                <Table stickyHeader aria-label="sticky table">
                                    <TableHead>
                                        <TableRow>{personTableColumns.map((row, key) => {
                                            return (
                                                <TableCell className={classes.header} key={key}  >
                                                    {row.label}
                                                </TableCell>
                                            );
                                        })}
                                            <TableCell className={classes.header}>Options</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {data && data.map((row: any, key: number) => {
                                            return (
                                                <TableRow
                                                    className={key % 2 !== 0 ? classes.colored : classes.notColored}
                                                    key={key} hover>
                                                    <TableCell style={{ cursor: 'pointer' }} onClick={() => handleUpdate(row.personId, row)}>
                                                        {row.personId}
                                                    </TableCell>
                                                    <TableCell>{row.firstName} </TableCell>
                                                    <TableCell>{row.lastName}</TableCell>
                                                    <TableCell>{row.phone}</TableCell>
                                                    <TableCell>{row.country}</TableCell>
                                                    <TableCell>
                                                        <Grid item container xs={12}>
                                                            <Grid item xs={4}><Chip label={'View'} /></Grid>
                                                            <Grid item xs={4}><Chip label={'Update'} /></Grid>
                                                            <Grid item xs={4}><Chip label={'Delete'} /></Grid>
                                                        </Grid>
                                                    </TableCell>
                                                </TableRow>
                                            );
                                        })}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Grid>
                    </div>
                </Grid >
            </Grid >
        </>
    )
}

export default Person;