import { Box, Grid, InputAdornment, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow, TextField, ThemeProvider, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import { palette } from "theme";
import useStyles from "./style";
import { personTableColumns } from "models/DataTableModel";
import * as personApi from "API/person";
import { PersonModel } from "models/PersonModel";
import { useLocation } from "react-router-dom";


const PersonUpdate = (prop: any) => {
    const classes = useStyles();
    const [personId, setPersonId] = useState();
    const [data, setData] = useState<PersonModel[]>();
    const { state } = useLocation();
    
    useEffect(() => {
        const fetch = async () => {
            try {
                const response = await personApi.getAllPersons();
                setData(response);
                console.log(response);
            } catch (error) {
                console.log("error getting persons", error);
            }
        };
        fetch();
    }, []);


    return (
        <>
            <Grid container justifyContent='center' alignItems={'center'}>
                <Grid item>
                    <Typography className={classes.title}> <strong>¡Welcome to the challenge!</strong></Typography>
                </Grid>
            </Grid>

            <div style={{ height: '1rem', width: '100%' }}></div>
            <div style={{ width: '100%', justifyContent: 'center', display: 'flex' }}>
                <Grid m={{ xs: 2, sm: 0 }} item xs={12}>

                </Grid>
            </div>


        </>
    )
}

export default PersonUpdate;