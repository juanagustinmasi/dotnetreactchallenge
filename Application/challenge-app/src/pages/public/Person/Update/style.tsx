import { Theme, alpha } from '@mui/material';
import { createStyles, makeStyles } from '@mui/styles';
import { palette } from 'theme';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
 
        tableContainer: {
            [theme.breakpoints.between('xs', 'sm')]: {
                width: `19rem`,
            },
            [theme.breakpoints.between('sm', 'md')]: {
                width: `40rem`,
            },
            [theme.breakpoints.up('md')]: {
                width: '100%',
            },
        },
        header: {
            fontSize: '1rem!important',
            color: `#707070!important`,
            fontStyle: 'bold',
            textAlign: 'center',
            [theme.breakpoints.between('xs', 'sm')]: {
                fontSize: '0.7rem!important',
                justifyContent: 'center',
            },

        },
        colored: {
            backgroundColor: alpha(palette.$primary_text, 0.2),
            height: '2.5rem',
            fontWeight: 'bold',
            padding: 0,
            '& .MuiTableCell-root': {
                paddingTop: 0,
                paddingBottom: 0,
                color: palette.$primary_text,
                fontWeight: 'bold',
                [theme.breakpoints.between('xs', 'sm')]: {
                    fontSize: '0.7rem!important',
                    justifyContent: 'center',
                },
            },
        },
        notColored: {
            backgroundColor: palette.$primary_text,
            height: '2.5rem',
            padding: 0,
            '& .MuiTableCell-root': {
                paddingTop: 0,
                paddingBottom: 0,
                [theme.breakpoints.between('xs', 'sm')]: {
                    fontSize: '0.7rem!important',
                    justifyContent: 'center',
                },

            },
        },
        title: {
            color: `${palette.$primary_text}!important`,
            fontSize: '1.2rem!important',
            fontStyle: 'bold'
        },
        text: {
            textAlign: 'center',
            color: `${palette.$primary_text}!important`,
            fontSize: '0.9rem!important',
            fontStyle: 'bold!important',
            letterSpacing: 0,
        },
 
        components: {
            MuiDataGrid: {
                styleOverrides: {
                    root: {
                        backgroundColor: 'green',
                    },
                },
            },
        },

    })
);
export default useStyles;