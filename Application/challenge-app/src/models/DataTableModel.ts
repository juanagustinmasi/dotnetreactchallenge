

export const personTableColumns = [
    { orderBy: 1, column: 'id', label: 'Id' },
    { orderBy: 2, column: 'firstName', label: 'First Name' },
    { orderBy: 3, column: 'lastName', label: 'Last Name' },
    { orderBy: 4, column: 'phone', label: 'Phone' },
    { orderBy: 5, column: 'country', label: 'Country' },
];
