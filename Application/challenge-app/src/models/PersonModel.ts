export interface PersonModel {
    personId?: string;
    firstName: string;
    lastName: string;
    phone: string;
    countryId: string;
}
