
import logo from './logo.svg';
import './App.css';
import React, { lazy } from 'react';
import { Route, Routes } from "react-router-dom";
import PersonUpdate from 'pages/public/Person/Update';

const Person = lazy(() => import('pages/public/Person'));
const Layout = lazy(() => import('pages/public/Layout'));
function App() {
  return (
    <Routes>
      <Route path='/' element={<Layout />} >
        <Route path='person' element={<Person />} >
          <Route path='update/:id' element={<PersonUpdate />} />
        </Route>
      </Route>
    </Routes>
  );
}

export default App;
