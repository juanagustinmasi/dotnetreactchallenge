import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';

export const createApi = (config?: AxiosRequestConfig): AxiosInstance => {
    const api = axios.create(config);
    return api;
}
export default createApi;