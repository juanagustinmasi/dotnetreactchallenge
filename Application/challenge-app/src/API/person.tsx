import axios from 'axios';
import createApi from 'API';
import { UUID } from 'crypto';
import { PersonModel } from 'models/PersonModel';

let endpoint = (window as { [key: string]: any })["runConfig"].api.challenge as string;

const api = createApi({
    baseURL: endpoint
});

export const getAllPersons = async () => {
    try {
        const response = await api.get(`get-all`);
        return response.data;
    } catch (error) {
        console.error('Error fetching all persons:', error);
        throw error;
    }
};

export const getPersonById = async (id: string) => {
    try {
        const response = await api.get(`read/${id}`);
        return response.data;
    } catch (error) {
        console.error(`Error fetching person with id ${id}:`, error);
        throw error;
    }
};

export const createPerson = async (person: PersonModel) => {
    try {
        const response = await api.post(`create`, person);
        return response.data;
    } catch (error) {
        console.error('Error creating person:', error);
        throw error;
    }
};

export const updatePerson = async (personData: PersonModel) => {
    try {
        const response = await api.post(`update`, personData);
        return response.data;
    } catch (error) {
        console.error(`Error updating person with id ${personData.personId}:`, error);
        throw error;
    }
};

export const deletePerson = async (id:string) => {
    try {
        const response = await axios.delete(`delete/${id}`);
        return response.data;
    } catch (error) {
        console.error(`Error deleting person with id ${id}:`, error);
        throw error;
    }
};
