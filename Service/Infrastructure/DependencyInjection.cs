﻿using Domain.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Infrastructure.Repositories;
namespace Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddPersistence(this IServiceCollection services)
        {

            services.AddScoped<IPerson, Persons>();

            return services;
        }
    }
}