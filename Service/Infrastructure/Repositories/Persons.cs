﻿using Dapper;
using Domain.Interfaces;
using Domain.Persons;
using Npgsql;
using System.Data;
using static System.Net.Mime.MediaTypeNames;

namespace Infrastructure.Repositories
{
    public class Persons : IPerson
    {
        protected NpgsqlConnection getConnection() => new("Server=localhost; Port=5432; Database=challenge; User Id=postgres; Password=dev;");
        public async Task<int> CreateAsync(Person req)
        {
            using var db = getConnection();
            return await db.ExecuteScalarAsync<int>("person_add", new
            {
                FirstName = req.FirstName,
                LastName = req.LastName,
                Phone = req.Phone,
                CountryId = req.CountryId,
            },
            commandType: CommandType.StoredProcedure);
        }

        public async Task<int> UpdateAsync(Person req)
        {
            using var db = getConnection();
            return await db.ExecuteScalarAsync<int>("person_update_bypersonid", new
            {
                PersonId = req.PersonId,
                FirstName = req.FirstName,
                LastName = req.LastName,
                Phone = req.Phone,
                CountryId = req.CountryId,
            },
            commandType: CommandType.StoredProcedure);
        }

        public async Task<ReadPerson> ReadAsync(Person req)
        {
            using var db = getConnection();
            var res = await db.QueryAsync<ReadPerson>("person_get_bypersonid", new
            {
                PersonId = req.PersonId
            },
            commandType: CommandType.StoredProcedure);

            return res.FirstOrDefault();
        }

        public async Task<int> DeleteAsync(Person req)
        {
            using var db = getConnection();
            return await db.ExecuteScalarAsync<int>("person_delete_bypersonid", new
            {
                PersonId = req.PersonId
            },
            commandType: CommandType.StoredProcedure);
        }

        public async Task<List<ReadPerson>> GetAllAsync()
        {
            using var db = getConnection();
            var res = await db.QueryAsync<ReadPerson>("person_get_all", commandType: CommandType.StoredProcedure);

            return res.ToList();
        }
    }
     
    
}
