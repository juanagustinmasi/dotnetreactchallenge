using Application.Persons.Create;
using Application.Persons.Delete;
using Application.Persons.GetAll;
using Application.Persons.Read;
using Application.Persons.Update;
using Infrastructure;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddScoped<CreateService>();
builder.Services.AddScoped<ReadService>();
builder.Services.AddScoped<UpdateService>();
builder.Services.AddScoped<DeleteService>();
builder.Services.AddScoped<GetAllService>();
builder.Services.AddPersistence();
AppContext.SetSwitch("Npgsql.EnableStoredProcedureCompatMode", true); //to run new version of postgresql


var app = builder.Build();
app.UseCors(x => x
              .AllowAnyOrigin()
              .AllowAnyMethod()
              .AllowAnyHeader());
// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())

app.UseSwagger();
app.UseSwaggerUI();


app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
