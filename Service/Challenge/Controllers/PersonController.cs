﻿using Application.Persons.Create;
using Application.Persons.Delete;
using Application.Persons.GetAll;
using Application.Persons.Read;
using Application.Persons.Update;
using Domain.Persons;
using Microsoft.AspNetCore.Mvc;

namespace Challenge.Controllers
{
    public class PersonController : Controller
    {
        private readonly CreateService _createService;
        private readonly ReadService _readService;
        private readonly GetAllService _getAllService;
        private readonly UpdateService _updateService;
        private readonly DeleteService _deleteService;

        public PersonController(CreateService createService, ReadService readService, UpdateService updateService, DeleteService deleteService, GetAllService getAllService)
        {
            _createService = createService;
            _readService = readService;
            _updateService = updateService;
            _deleteService = deleteService;
            _getAllService = getAllService;
        }
        [HttpPost("create")]

        public async Task <int> CreateAsync([FromBody] CreateRequest req )
        {
            return await _createService.CreateAsync(req);
        }

        [HttpGet("read")]

        public async Task<ReadPerson> ReadAsync( ReadRequest req)
        {
            return await _readService.ReadAsync(req);
        }

        [HttpGet("get-all")]
        public async Task<ActionResult<List<ReadPerson>>> GetAllAsync()
        {
        var persons =  await _getAllService.GetAllAsync();
            if (persons == null || persons.Count == 0)
            {
                return NotFound();
            }
            return Ok(persons);
        }


        [HttpPost("update")]
        public async Task<int> UpdateAsync([FromBody] UpdateRequest req)
        {
            return await _updateService.UpdateAsync(req);
        }

        [HttpDelete("delete")]

        public async Task<int> DeleteAsync([FromBody] DeleteRequest req)
        {
            return await _deleteService.DeleteAsync(req);
        }

    }
}
