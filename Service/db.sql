CREATE EXTENSION IF NOT EXISTS "uuid-ossp"; -- for the uuid methods

CREATE TABLE IF NOT EXISTS "country"
(
    countryid uuid not null default uuid_generate_v4(),
    description character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT countryid_pkey PRIMARY KEY (countryid)

)

CREATE TABLE IF NOT EXISTS "person"
(
    personid uuid   not null default uuid_generate_v4(),
    firstname character varying(255) COLLATE pg_catalog."default" NOT NULL,
    lastname character varying(255) COLLATE pg_catalog."default" NOT NULL,
    phone character varying(255) COLLATE pg_catalog."default" NOT NULL,
    countryid uuid NOT NULL,
    CONSTRAINT person_pkey PRIMARY KEY (personid),
    CONSTRAINT person_countryid_fkey FOREIGN KEY (countryid)
        REFERENCES public.country (countryid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

insert into country(description) values('Argentine'), ('Germany'),('Brazil')  --insert 3 countries

CREATE OR REPLACE FUNCTION public.person_add(  -- stored procedure to add person
	"FirstName" text,
	"LastName" text,
	"Phone" text,
	"CountryId" uuid)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
begin

   
 INSERT INTO public."person"(
	firstname, lastname, phone, countryid)
	VALUES ((trim("FirstName")), (trim("LastName")),"Phone","CountryId");
	return 0;
end;
$BODY$;

select person_get_bypersonid('13c8d535-60e4-4350-9688-f2275f780998')
drop function public.person_get_bypersonid(uuid)
CREATE OR REPLACE FUNCTION public.person_get_bypersonid( --stored procedure to get person by userid
	"PersonId" uuid)
    RETURNS TABLE(	
	"FirstName" character varying(255),
	"LastName" character varying(255),
	"Phone" character varying(255),
	"Country" character varying(255) ) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
begin
 
 	return query select 
	p.firstname as FirstName,
	p.lastname as LastName,
	p.phone as Phone,
	c.description  as Country
	from person p 
	inner join country c on c.countryid = p.countryid
	where p.personid = "PersonId" ;
			
end;
$BODY$;

 
CREATE OR REPLACE FUNCTION public.person_update_bypersonid(  --stored procedure to update person by userid
	"PersonId" uuid,
	"FirstName" text,
	"LastName" text,
	"Phone" text,
	"CountryId" uuid)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
    UPDATE person
	SET firstname = "FirstName",
		lastname = "LastName",
		phone = "Phone",
		countryId = "CountryId"
		
	where personid = "PersonId";
	return 0; -- zero means ok!
END;
$BODY$;


CREATE OR REPLACE FUNCTION public.person_delete_bypersonid( -- sp to delete person by userid
	"PersonId" uuid)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
     
    DELETE FROM public.person
    WHERE personid = "PersonId";
    RETURN 0;
END;
$BODY$;


 CREATE OR REPLACE FUNCTION public.person_get_all()
    RETURNS TABLE("PersonId" uuid, "FirstName" character varying, "LastName" character varying, "Phone" character varying, "Country" character varying) 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
begin
 
 	return query select 
	p.personid as PersonId,
	p.firstname as FirstName,
	p.lastname as LastName,
	p.phone as Phone,
	c.description  as Country
	from person p 
	inner join country c on c.countryid = p.countryid ;
			
end;
$BODY$;
 