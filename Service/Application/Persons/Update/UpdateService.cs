﻿using Domain.Interfaces;
using Domain.Persons;

namespace Application.Persons.Update
{
    public class UpdateService
    {
        private readonly IPerson _personRepository;
        public UpdateService(IPerson personRepository)
        {
            _personRepository = personRepository;
        }

        public async Task<int> UpdateAsync(UpdateRequest request)
        {
            return await _personRepository.UpdateAsync(new Person
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                PersonId = request.PersonId,
                CountryId = request.CountryId,
                Phone = request.Phone
            });
        }
    }
}
