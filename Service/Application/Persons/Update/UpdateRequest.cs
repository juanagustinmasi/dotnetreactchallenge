﻿
namespace Application.Persons.Update
{
    public class UpdateRequest
    {
        public Guid PersonId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Phone { get; set; }
        public Guid CountryId { get; set; }
    }
}
