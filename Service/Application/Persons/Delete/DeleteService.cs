﻿
using Domain.Interfaces;
using Domain.Persons;

namespace Application.Persons.Delete
{
    public class DeleteService
    {
        private readonly IPerson _personRepository;
        public DeleteService(IPerson personRepository)
        {
            _personRepository = personRepository;
        }

        public async Task<int> DeleteAsync(DeleteRequest request)
        {
            return await _personRepository.DeleteAsync(new Person
            {
            PersonId = request.PersonId
            });
        }
    }
}
