﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Persons.Delete
{
    public class DeleteRequest
    {
        public Guid PersonId { get; set; }
    }
}
