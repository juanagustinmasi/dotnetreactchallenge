﻿using Domain.Interfaces;
using Domain.Persons;


namespace Application.Persons.Create
{
    public class CreateService
    {
        private readonly IPerson _personRepository;
        public CreateService(IPerson personRepository)
        {
            _personRepository = personRepository;
        }

        public async Task<int> CreateAsync(CreateRequest request)
        {
            return await _personRepository.CreateAsync(new Person
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                CountryId = request.CountryId,
                Phone = request.Phone
            });
        }
    }
}
