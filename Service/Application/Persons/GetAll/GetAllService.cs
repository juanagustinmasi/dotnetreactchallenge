﻿using Domain.Interfaces;
using Domain.Persons;


namespace Application.Persons.GetAll
{
    public class GetAllService
    {
        private readonly IPerson _personRepository;
        public GetAllService(IPerson personRepository)
        {
            _personRepository = personRepository;
        }

        public async Task<List<ReadPerson>> GetAllAsync()
        {
            return await _personRepository.GetAllAsync();
        }
    }
}
