﻿
using Domain.Interfaces;
using Domain.Persons;


namespace Application.Persons.Read
{
    public class ReadService
    {
        private readonly IPerson _personRepository;
        public ReadService(IPerson personRepository)
        {
            _personRepository = personRepository;
        }

        public async Task<ReadPerson> ReadAsync(ReadRequest request)
        {
            return await _personRepository.ReadAsync(new Person
            {
                PersonId = request.PersonId
            });
        }
    }
}
