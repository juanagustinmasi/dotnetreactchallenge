﻿
namespace Application.Persons.Read
{
    public class ReadRequest
    {
        public Guid PersonId { get; set; }
    }
}
