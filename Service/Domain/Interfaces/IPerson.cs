﻿using Domain.Persons;

namespace Domain.Interfaces
{
    public interface IPerson
    {
        Task<int> CreateAsync(Person req);
        Task<int> UpdateAsync(Person req);
        Task<ReadPerson> ReadAsync(Person req);
        Task<List<ReadPerson>> GetAllAsync();
        Task<int> DeleteAsync(Person req);
    }
}
