﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Countries
{
    public class Countries
    {
        public Guid CountryId { get; set; }
        public string? Description { get; set; }
    }
}
