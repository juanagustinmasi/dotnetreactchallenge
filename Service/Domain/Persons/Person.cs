﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Persons
{
    public class Person
    {
        public Guid PersonId{ get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Phone { get; set; }
        public Guid CountryId { get; set; }

        public static Person UpdateAsync(Guid personId, string firstName, string lastName, string phone, Guid countryId)
        {
            return new Person(personId, firstName, lastName, phone, countryId);
        }

        public Person() { }
        public Person(Guid personId, string? firstName, string? lastName, string? phone, Guid countryId)
        {
            PersonId = personId;
            FirstName = firstName;
            LastName = lastName;
            Phone = phone;
            CountryId = countryId;
        }
    }
        
}
